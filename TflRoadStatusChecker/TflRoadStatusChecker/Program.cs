﻿using System.Linq;
using Autofac;
using TflRoadStatusChecker.RestApi.Contracts;

namespace TflRoadStatusChecker
{
    class Program
    {
        static int Main(string[] args)
        {
            var app = RoadStatusCheckerContainer.Container.Resolve<IRoadStatusChecker>();
            string parameter;

            if ((parameter = ParseArgs(args)) != null)
            {
                return app.GetRoadCurrentStatus(parameter);
            }

            return -1;
        }

        private static string ParseArgs(string[] args)
        {
            if (args != null && args.Length == 1)
            {
                return args.First();
            }

            return null;
        }
    }
}
