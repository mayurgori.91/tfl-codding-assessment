﻿using NUnit.Framework;
using TflRoadStatusChecker.RestApi.Contracts;
using TflRoadStatusChecker.RestApi.Implementation;
using TflRoadStatusChecker.RestApi.Models;

namespace TflRoadStatusChecker.UnitTest
{
    [TestFixture]
    public class DisplayTests
    {
        private IDisplay _display;

        [SetUp]
        public void Setup()
        {
            _display = new Display();
        }

        [Test]
        public void VerifyDisplaySuccessMessageMethodReturnsTextAsExpected()
        {
            // Arrange
            var input = new RoadStatusServiceResponse
            {
                displayName = "A2",
                statusSeverity = "Good",
                statusSeverityDescription = "No Exceptional Delays"
            };

            var expectedOutput = "The status of the A2 is as follows\r\n\t Road Status is Good\r\n\t Road Status Description is No Exceptional Delays\r\n";

            // Act
            this._display.DisplaySuccessMessage(input);

            // Assert
            Assert.AreEqual(expectedOutput, this._display.OutputMessage.ToString());
        }

        [Test]
        public void VerifyDisplayNotFoundMessageMethodReturnsTextAsExpected()
        {
            // Arrange
            var input = "A223";
            var expectedOutput = "A223 is not a valid road\r\n";

            // Act
            this._display.DisplayNotFoundMessage(input);

            // Assert
            Assert.AreEqual(expectedOutput, this._display.OutputMessage.ToString());
        }
    }
}
