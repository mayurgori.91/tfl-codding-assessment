﻿using System.Linq;
using System.Net;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using TflRoadStatusChecker.RestApi.Contracts;
using TflRoadStatusChecker.RestApi.HttpClient;
using TflRoadStatusChecker.RestApi.Models;

namespace TflRoadStatusChecker.UnitTest
{
    [TestFixture]
    public class RestClientTests
    {
        private Mock<IConfig> _mockConfig;

        private IRestApiClient _restApiClient;

        [SetUp]
        public void Setup()
        {
            _mockConfig = new Mock<IConfig>();
            _mockConfig.Setup(x => x.Url).Returns("https://unittesting.co.uk/");
        }

        [Test]
        public void VerifyGetStatusMethodReturnsCorrectDataForOkStatus()
        {
            // Arrange
            var expectedDisplayName = "A2";
            var expectedStatusSeverity = "Good";
            var expectedStatusSeverityDescription = "No Exceptional Delays";
            this._restApiClient = new RestApiClient(this._mockConfig.Object, MockHttpClientHandler.GetMockClient(HttpStatusCode.OK));

            // Act
            var result = this._restApiClient.Get("A2");

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(MockHttpClientHandler.GetServiceResponse(HttpStatusCode.OK), result);
            var resultModel = JsonConvert.DeserializeObject<RoadStatusServiceResponse[]>(result)?.First();
            Assert.IsNotNull(resultModel);
            Assert.AreEqual(resultModel.displayName, expectedDisplayName);
            Assert.AreEqual(resultModel.statusSeverity, expectedStatusSeverity);
            Assert.AreEqual(resultModel.statusSeverityDescription, expectedStatusSeverityDescription);
        }

        [Test]
        public void VerifyGetStatusMethodReturnsCorrectDataForNotFoundStatus()
        {
            // Arrange
            var expectedHttpStatusCode = "404";
            this._restApiClient = new RestApiClient(this._mockConfig.Object, MockHttpClientHandler.GetMockClient(HttpStatusCode.NotFound));

            // Act
            var result = this._restApiClient.Get("A2");

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(MockHttpClientHandler.GetServiceResponse(HttpStatusCode.NotFound), result);
            var resultModel = JsonConvert.DeserializeObject<NotFoundServiceResponse>(result);
            Assert.IsNotNull(resultModel);
            Assert.AreEqual(resultModel.httpStatusCode, expectedHttpStatusCode);
        }
    }
}
