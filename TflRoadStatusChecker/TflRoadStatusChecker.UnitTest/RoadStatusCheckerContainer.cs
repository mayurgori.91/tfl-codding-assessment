﻿using Autofac;
using TflRoadStatusChecker.RestApi;

namespace TflRoadStatusChecker.UnitTest
{
    public static class RoadStatusCheckerContainer
    {
        public static Autofac.IContainer Container { get; set; }

        static RoadStatusCheckerContainer()
        {
            Container = Build();
        }

        private static Autofac.IContainer Build()
        {
            var container = new ContainerBuilder();

            container.RegisterModule(new RoadStatusRestApiContainer());

            return container.Build();
        }
    }
}