﻿using System.Net;
using Moq;
using NUnit.Framework;
using TflRoadStatusChecker.RestApi.Contracts;
using TflRoadStatusChecker.RestApi.HttpClient;
using TflRoadStatusChecker.RestApi.Implementation;
using TflRoadStatusChecker.RestApi.Models;

namespace TflRoadStatusChecker.UnitTest
{
    [TestFixture]
    public class RoadStatusCheckerTests
    {
        private Mock<IDisplay> _mockDisplay;

        private Mock<IConfig> _mockConfig;

        private IRestApiClient _restApiClient;

        private IRoadStatusChecker _roadStatusChecker;

        [SetUp]
        public void Setup()
        {
            _mockDisplay = new Mock<IDisplay>();
            _mockConfig = new Mock<IConfig>();
            _mockConfig.Setup(x => x.Url).Returns("https://unittesting.co.uk/");
        }

        [TestCase(HttpStatusCode.OK, "A2", 0)]
        [TestCase(HttpStatusCode.NotFound, "A223", 1)]
        [TestCase(HttpStatusCode.Forbidden, "A223", -1)]
        [TestCase(HttpStatusCode.Forbidden, "", -1)]
        public void VerifyGetRoadCurrentStatusMethodReturnsNumberBasedOnStatus(HttpStatusCode status, string roadName, int expectedNumber)
        {
            // Arrange
            this._restApiClient = new RestApiClient(this._mockConfig.Object, MockHttpClientHandler.GetMockClient(status));
            _roadStatusChecker = new RoadStatusChecker(this._restApiClient, _mockDisplay.Object);

            // Act
            var result = this._roadStatusChecker.GetRoadCurrentStatus(roadName);

            // Assert
            Assert.AreEqual(expectedNumber, result);
            this.StatusBasedDisplayMethodCheck(status);
        }

        private void StatusBasedDisplayMethodCheck(HttpStatusCode status)
        {
            switch (status)
            {
                case HttpStatusCode.OK:
                    this._mockDisplay.Verify(c => c.DisplaySuccessMessage(It.IsAny<RoadStatusServiceResponse>()), Times.Once);
                    this._mockDisplay.Verify(c => c.DisplayNotFoundMessage(It.IsAny<string>()), Times.Never);
                    break;
                case HttpStatusCode.NotFound:
                    this._mockDisplay.Verify(c => c.DisplayNotFoundMessage(It.IsAny<string>()), Times.Once);
                    this._mockDisplay.Verify(c => c.DisplaySuccessMessage(It.IsAny<RoadStatusServiceResponse>()), Times.Never);
                    break;
                default:
                    this._mockDisplay.Verify(c => c.DisplaySuccessMessage(It.IsAny<RoadStatusServiceResponse>()), Times.Never);
                    this._mockDisplay.Verify(c => c.DisplayNotFoundMessage(It.IsAny<string>()), Times.Never);
                    break;
            }
        }
    }
}
