# Project Title
 
TfL Coding Assessment
 
### Prerequisites
Visual Studio 2017 or higher
Powershell
 
### How to open & build solution
 
1. Download/Clone the solution from given link.
2. Open the solution file TflRoadStatusChecker.sln using visual studio
3. Once solution is opened Right click on solution and select restore nuget package(so that if any package is missing it will install)
4. Build Soultion
5. On Succesful build it will create package file at path : c:/RoadStatusCheckerDeploy
 
### How to run app
1. Open command prompt and change working directory to - c:/RoadStatusCheckerDeploy
2. Once step number one is done then run command :  RoadStatusCheckerDeploy A2
3. On running command in step number 2, it will show successful scenario output.
4. To test not found scenario run command : RoadStatusCheckerDeploy A223
5. On running command as per step number 4, it will show expected output.
 
## How to run unit test cases
1. Open solution in similar way as described in above section(How to open & build solution)
2. Build solution once
3. Open test explorer & wait for 1-2 minute, so that text explorer can load unit test project
4. You will be able to see test project - TflRoadStatusChecker.UnitTest is loaded
5. Right click on test project & select Run option
6. It will run all unit test cases & it will show result.
 
Note - For better designing approach decoupling is given high priority in this solution while developing, so that any changes can be added/modified easily if required.