﻿using Autofac;
using TflRoadStatusChecker.RestApi.Contracts;
using TflRoadStatusChecker.RestApi.HttpClient;
using TflRoadStatusChecker.RestApi.Implementation;

namespace TflRoadStatusChecker.RestApi
{
    public class RoadStatusRestApiContainer : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c => new Config()).As<IConfig>();

            builder.Register(c => new Display()).As<IDisplay>();

            builder.Register(c => new RestApiClient(c.Resolve<IConfig>(), new System.Net.Http.HttpClient()))
                .As<IRestApiClient>();

            builder.Register(c => new RoadStatusChecker(c.Resolve<IRestApiClient>(), c.Resolve<IDisplay>()))
                .As<IRoadStatusChecker>();
        }
    }
}