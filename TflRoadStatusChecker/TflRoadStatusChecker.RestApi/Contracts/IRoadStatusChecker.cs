﻿namespace TflRoadStatusChecker.RestApi.Contracts
{
    public interface IRoadStatusChecker
    {
        int GetRoadCurrentStatus(string road);
    }
}