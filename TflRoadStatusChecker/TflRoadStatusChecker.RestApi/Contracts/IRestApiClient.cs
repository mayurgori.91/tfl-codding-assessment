﻿using System.Net;

namespace TflRoadStatusChecker.RestApi.Contracts
{
    public interface IRestApiClient
    {
        HttpStatusCode StatusCode { get; }

        string Get(string road);
    }
}