﻿using System.Text;
using TflRoadStatusChecker.RestApi.Models;

namespace TflRoadStatusChecker.RestApi.Contracts
{
    public interface IDisplay
    {
        StringBuilder OutputMessage { get; set; }

        void DisplaySuccessMessage(RoadStatusServiceResponse response);

        void DisplayNotFoundMessage(string roadName);
    }
}