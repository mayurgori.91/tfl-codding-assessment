﻿using System.Net;
using System.Net.Http;
using TflRoadStatusChecker.RestApi.Contracts;

namespace TflRoadStatusChecker.RestApi.HttpClient
{
    public class RestApiClient : IRestApiClient
    {
        private readonly IConfig config;

        private readonly System.Net.Http.HttpClient httpClient;
        
        public HttpStatusCode StatusCode { get; private set; }

        public RestApiClient(IConfig config, System.Net.Http.HttpClient httpClient)
        {
            this.config = config;
            this.httpClient = httpClient;
        }

        public string Get(string road)
        {
            var content = string.Empty;

            using (httpClient)
            {
                var requestUrl = CreateUrl(road);

                var httpContent = new HttpRequestMessage(HttpMethod.Get, requestUrl);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                var response = httpClient.SendAsync(httpContent).Result;

                this.StatusCode = response.StatusCode;

                if (response.StatusCode != HttpStatusCode.OK && 
                    response.StatusCode != HttpStatusCode.NotFound && response.StatusCode != HttpStatusCode.Forbidden)
                {
                    throw new HttpRequestException($"Error for your request, http status code {response.StatusCode}");
                }

                content = response.Content.ReadAsStringAsync().Result;
            }

            return content;
        }

        private string CreateUrl(string road)
        {
            return string.Format(config.Url + "{0}?app_id={1}&app_key={2}", road, config.AppID, config.AppKey);
        }
    }
}