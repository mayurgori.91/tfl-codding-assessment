﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using TflRoadStatusChecker.RestApi.Contracts;
using TflRoadStatusChecker.RestApi.Models;

namespace TflRoadStatusChecker.RestApi.Implementation
{
    public class RoadStatusChecker : IRoadStatusChecker
    {
        private readonly IRestApiClient _restApiClient;

        public IDisplay Display { get; }

        public RoadStatusServiceResponse ServiceResponse { get; set; }

        public NotFoundServiceResponse HttpNotFoundError { get; set; }

        public RoadStatusChecker(IRestApiClient restApiClient, IDisplay display)
        {
            this._restApiClient = restApiClient;
            this.Display = display;
        }

        public int GetRoadCurrentStatus(string road)
        {
            try
            {
                var roadStatus = string.IsNullOrWhiteSpace(road) ? null : _restApiClient.Get(road);
                if (!string.IsNullOrWhiteSpace(roadStatus?.Trim()))
                {
                    switch (_restApiClient.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            ServiceResponse = JsonConvert.DeserializeObject<RoadStatusServiceResponse[]>(roadStatus)?.First();
                            Display.DisplaySuccessMessage(ServiceResponse);
                            return 0;
                        case HttpStatusCode.NotFound:
                            HttpNotFoundError = JsonConvert.DeserializeObject<NotFoundServiceResponse>(roadStatus);
                            Display.DisplayNotFoundMessage(road);
                            return 1;
                        default:
                            return -1;
                    }
                }
            }
            catch (Exception)
            {
                /* We can implement logging as per requirement */
            }

            return -1;
        }
    }
}