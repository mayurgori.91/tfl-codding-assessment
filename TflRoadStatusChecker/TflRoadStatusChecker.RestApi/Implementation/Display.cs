﻿using System;
using System.Text;
using TflRoadStatusChecker.RestApi.Contracts;
using TflRoadStatusChecker.RestApi.Models;

namespace TflRoadStatusChecker.RestApi.Implementation
{
    public class Display : IDisplay
    {
        public StringBuilder OutputMessage { get; set; } = new StringBuilder();

        public void DisplaySuccessMessage(RoadStatusServiceResponse response)
        {
            this.OutputMessage.AppendLine($"The status of the {response.displayName} is as follows");
            this.OutputMessage.AppendLine($"\t Road Status is {response.statusSeverity}");
            this.OutputMessage.AppendLine($"\t Road Status Description is {response.statusSeverityDescription}");
            Console.WriteLine(this.OutputMessage.ToString());
        }

        public void DisplayNotFoundMessage(string roadName)
        {
            this.OutputMessage.AppendLine($"{roadName} is not a valid road");
            Console.WriteLine(this.OutputMessage.ToString());
        }
    }
}