﻿using System.Collections.Specialized;
using System.Configuration;
using TflRoadStatusChecker.RestApi.Contracts;

namespace TflRoadStatusChecker.RestApi.Implementation
{
    public class Config : IConfig
    {
        public Config()
        {
            var settings = (NameValueCollection)ConfigurationManager.GetSection("RoadStatusAppSettings");

            this.Url = settings["url"] ?? "";
            this.AppID = settings["app_id"] ?? "";
            this.AppKey = settings["app_key"] ?? "";
        }

        public string Url { get; set; }

        public string AppID { get; set; }

        public string AppKey { get; set; }
    }
}